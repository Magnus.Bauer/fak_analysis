[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.physik.uni-muenchen.de%2FMagnus.Bauer%2Ffak_analysis/433b27076fb8f08361955b743f91d0ea31ef5616?filepath=FAK_analysis.ipynb)

Analysis scripts for 'Structural and mechanistic insights into mechanoactivation of Focal Adhesion Kinase'
--------

For interacting directly with the jupyter notebook click [here for using binder](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.physik.uni-muenchen.de%2FMagnus.Bauer%2Ffak_analysis/433b27076fb8f08361955b743f91d0ea31ef5616?filepath=FAK_analysis.ipynb). Or click on the binder badge above.

